This module provides integration with the Apifon email delivery service.

The core module provides basic configuration and API integration. You can set
your API key, view, create and delete lists, set a rule for subscribing a user
to a desired list and also a custom block for registering to the newsletter.


## Installation Notes
  * You need to have an Apifon API Key.
  * The Apifon API library is included in the module

## Configuration
  1. Direct your browser to admin/config/services/apifon to configure the
  module.