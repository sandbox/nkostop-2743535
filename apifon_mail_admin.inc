<?php

/*
 * Form for creating new list
 */
function apifon_mail_create_list_form($form, &$form_state){

    $form['apifon_mail_list_name'] = [
        '#type' => 'textfield',
        '#title' => t('List name'),
        '#description' => t('Enter the name of the new subscribers list that you want to create'),
    ];

    return (system_settings_form($form));
}