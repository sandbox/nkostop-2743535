<?php

class ApifonApi_Endpoint_ListFields extends ApifonApi_Base
{
    /**
     * Get fields from a certain mail list
     * 
     * Note, the results returned by this endpoint can be cached.
     * 
     * @param string $listUid
     * @return ApifonApi_Http_Response
     */
    public function getFields($listUid)
    {
        $client = new ApifonApi_Http_Client(array(
            'method'        => ApifonApi_Http_Client::METHOD_GET,
            'url'           => $this->config->getApiUrl(sprintf('lists/%s/fields', $listUid)),
            'paramsGet'     => array(),
            'enableCache'   => true,
        ));
        
        return $response = $client->request();
    }
}