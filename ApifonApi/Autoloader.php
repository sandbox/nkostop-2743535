<?php

class ApifonApi_Autoloader
{
    /**
     * The registrable autoloader
     * 
     * @param string $class
     */
    public static function autoloader($class)
    {
        if (strpos($class, 'ApifonApi') === 0) {
            $className = str_replace('_', '/', $class);
            $className = substr($className, 10);

            if (is_file($classFile = dirname(__FILE__) . '/'. $className.'.php')) {
                require_once($classFile);
            }
        }
    }
    
    /**
     * Registers the ApifonApi_Autoloader::autoloader()
     */
    public static function register()
    {
        spl_autoload_register(array('ApifonApi_Autoloader', 'autoloader'));
    }
}