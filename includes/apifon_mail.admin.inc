<?php

/*
 * General Settings for Apifone Mail service
 */
function apifon_mail_admin_settings($form, &$form_state){
    $form['apifon_mail_public_key'] = [
        '#type' => 'textfield',
        '#title' => t('Public API Key'),
        '#required' => TRUE,
        '#default_value' => variable_get('apifon_mail_public_key'),
        '#description' => t('Enter your public API key'),
    ];
    $form['apifon_mail_private_key'] = [
        '#type' => 'textfield',
        '#title' => t('Private API Key'),
        '#required' => TRUE,
        '#default_value' => variable_get('apifon_mail_private_key'),
        '#description' => t('Enter your Private API key'),
    ];

    return (system_settings_form($form));

}

/*
 * Show all current suscribers lists
 */
function apifon_mail_lists_overview_page() {
    // require the setup which has registered the autoloader
    apifon_mail_api_setup();

    // create the lists endpoint:
    $endpoint = new ApifonApi_Endpoint_Lists();

    // CREATE THE ENDPOINT
    $endpointSubscribers = new ApifonApi_Endpoint_ListSubscribers();

    /*===================================================================================*/

// GET ALL ITEMS
    $response = $endpoint->getLists($pageNumber = 1, $perPage = 10);

// DISPLAY RESPONSE
    //TODO: Render as table MISSING FIELDS
    $header = array('Name', 'Display Name', 'Subscribers count', 'Opt in', 'Opt out', 'Merged', 'Created date', 'Updated date', 'Actions');
    $rows = array();

    foreach ($response->body['data']['records'] as $key => $value){
        $responseSubscribers = $endpointSubscribers->getSubscribers($value['general']['list_uid'], $pageNumber = 1, $perPage = 10);
        $rows[] = array(
            $value['general']['name'],
            $value['general']['display_name'],
            $responseSubscribers->body['data']['count'],
            'notyet',
            'notyet',
            'notyet',
            'notyet',
            'notyet',
            '<a href="/admin/config/services/apifon_mail/lists/'.$value['general']['list_uid'].'/delete">DELETE</a>'
        );
    }
    return theme('table', array('header' => $header, 'rows' => $rows));
}

/*
 * Form for creating new list
 */
function apifon_mail_create_list_form($form, &$form_state){
    //TODO: Add all required fields

    $form['apifon_mail_list_name'] = [
        '#type' => 'textfield',
        '#title' => t('List name'),
        '#required' => TRUE,
        '#description' => t('Enter the name of the new subscribers list that you want to create'),
    ];
    $form['apifon_mail_list_description'] = [
        '#type' => 'textfield',
        '#title' => t('Description'),
        '#required' => TRUE,
        '#description' => t('Enter a description of the new subscribers list that you want to create'),
    ];
    $form['apifon_mail_list_from_name'] = [
        '#type' => 'textfield',
        '#title' => t('From name'),
        '#required' => TRUE,
        '#description' => t('Enter the name you want for the email to show from'),
    ];
    $form['apifon_mail_list_from_email'] = [
        '#type' => 'textfield',
        '#title' => t('From email'),
        '#required' => TRUE,
        '#description' => t('Enter the email you want for the email to show from'),
    ];
    $form['apifon_mail_list_reply_to'] = [
        '#type' => 'textfield',
        '#title' => t('Reply-to address'),
        '#required' => TRUE,
        '#description' => t('Enter the reply to email you want your receivers to reply'),
    ];
    $form['apifon_mail_list_subject'] = [
        '#type' => 'textfield',
        '#title' => t('Subject'),
        '#required' => FALSE,
        '#description' => t('Enter the subject you want for your emails'),
    ];

    $form['submit_button'] = array(
        '#type' => 'submit',
        '#value' => t('Create list'),
    );

    return $form;
}

/*
 * Implements form_validate() and form_submit() for create_list_form
 */
function apifon_mail_create_list_form_validate($form, &$form_state) {
    //TODO: make it full
    if(empty($form_state['values']['apifon_mail_list_name'])){
        form_set_error('list', t('List name must not be empty.'));
    }
}

function apifon_mail_create_list_form_submit($form, &$form_state) {
    //TODO: fix all fields
    // require the setup which has registered the autoloader
    apifon_mail_api_setup();

    // create the lists endpoint:
    $endpoint = new ApifonApi_Endpoint_Lists();

    /*===================================================================================*/

    // create a new list
// please see countries.php example file for a list of allowed countries/zones for list company
    $response = $endpoint->create(array(
        // required
        'general' => array(
            'name'          => $form_state['values']['apifon_mail_list_name'], // required
            'description'   => $form_state['values']['apifon_mail_list_description'], // required
        ),
        // required
        'defaults' => array(
            'from_name' => $form_state['values']['apifon_mail_list_from_name'], // required
            'from_email'=> $form_state['values']['apifon_mail_list_from_email'], // required
            'reply_to'  => $form_state['values']['apifon_mail_list_reply_to'], // required
            'subject'   => $form_state['values']['apifon_mail_list_subject'],
        ),
        // optional
        'notifications' => array(
            // notification when new subscriber added
            'subscribe'         => 'yes', // yes|no
            // notification when subscriber unsubscribes
            'unsubscribe'       => 'yes', // yes|no
            // where to send the notifications.
            'subscribe_to'      => 'johndoe@doe.com',
            'unsubscribe_to'    => 'johndoe@doe.com',
        ),
        // optional, if not set customer company data will be used
        'company' => array(
            'name'      => 'John Doe INC', // required
            'country'   => 'United States', // required
            'zone'      => 'New York', // required
            'address_1' => 'Some street address', // required
            'address_2' => '',
            'zone_name' => '', // when country doesn't have required zone.
            'city'      => 'New York City',
            'zip_code'  => '10019',
        ),
    ));

    $form_state['redirect'] = 'admin/config/services/apifon_mail/lists';
    drupal_set_message('The '.$form_state['values']['apifon_mail_list_name'].' list has been saved at '.date("d/m/Y H:i:s"));

}

/*
 * Function for deleting a whole list
 */
function apifon_mail_lists_delete($listId){
    //TODO: change it to form for confirmation of delete action
    // require the setup which has registered the autoloader
    apifon_mail_api_setup();

    // create the lists endpoint:
    $endpoint = new ApifonApi_Endpoint_Lists();

    /*===================================================================================*/

    $response = $endpoint->delete($listId);

    // DISPLAY RESPONSE
    if($response->body['status']=='success'){
        drupal_set_message('List with Unique Id '.$listId.' deleted.');
    } else{
        drupal_set_message('Error: '.$response->body['error']);
    }

    drupal_goto('admin/config/services/apifon_mail/lists');
}