<?php

/**
 * Basic RULES implementation
 */

/**
 * Implements hook_rules_action_info()
 *
 * Declares any meta-date about actions for Rules.
 */
function apifon_mail_rules_action_info(){

    $actions = array(
        'apifon_mail_action_subscribe_user_to_list' => array(
            'label' => t('Subscribe user to subscription list'),
            'group' => 'Apifon Mail',
            'parameter' => array(
                'account' => array(
                    'type' => 'user',
                    'label' => t('User to subscribe'),
                    'save' => FALSE,
                ),
                'subscribers_list' => array(
                    'type' => 'text',
                    'label' => t('Please select the list to subscibe the user'),
                    'options list' => 'rules_apifon_mail_subscribers_list',
                    'restriction' => 'input',
                ),
            ),
        ),
        'apifon_mail_action_update_user_to_list' => array(
            'label' => t('Update user of subscription list'),
            'group' => 'Apifon Mail',
            'parameter' => array(
                'account' => array(
                    'type' => 'user',
                    'label' => t('User to update'),
                    'save' => FALSE,
                ),
            ),
        ),
    );

    return $actions;
}

/**
 * The action function for 'apifon_mail_action_subscribe_user_to_list'.
 */
function apifon_mail_action_subscribe_user_to_list($account, $subscribers_list){
    // require the setup which has registered the autoloader
    apifon_mail_api_setup();

    // CREATE THE ENDPOINT
    $endpoint = new ApifonApi_Endpoint_ListSubscribers();

    // ADD SUBSCRIBER
    $response = $endpoint->create($subscribers_list, array(
        'EMAIL'    => $account->mail, // the confirmation email will be sent!!! Use valid email address
        'FNAME'    => $account->name,
        'LNAME'    => $acount->name
    ));
}

/**
 * The action function for 'apifon_mail_action_update_user_to_list'.
 */
function apifon_mail_action_update_user_to_list($account){
    // require the setup which has registered the autoloader
    apifon_mail_api_setup();

    // CREATE THE ENDPOINT
    $endpoint = new ApifonApi_Endpoint_ListSubscribers();

    $response = $endpoint->emailSearchAllLists($account->mail);

    watchdog('search_list','response_body: %response',array('%response' => $response->body));

//    $update = $endpoint->update('LIST-UNIQUE-ID', 'SUBSCRIBER-UNIQUE-ID', array(
//        'EMAIL'    => $account->mail,
//        'FNAME'    => $account->name,
//        'LNAME'    => $account->name
//    ));
}

function rules_apifon_mail_subscribers_list(){
    // require the setup which has registered the autoloader
    apifon_mail_api_setup();

    // create the lists endpoint:
    $endpoint = new ApifonApi_Endpoint_Lists();

    // GET ALL ITEMS
    $response = $endpoint->getLists($pageNumber = 1, $perPage = 10);

    $rows = array();

    foreach ($response->body['data']['records'] as $key => $value){
        $rows[$value['general']['list_uid']] = $value['general']['display_name'];
    }

    return $rows;
}