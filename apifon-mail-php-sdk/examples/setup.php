<?php

//exit('COMMENT ME TO TEST THE EXAMPLES!');
 
// require the autoloader class
require_once dirname(__FILE__) . '/../ApifonApi/Autoloader.php';

// register the autoloader.
ApifonApi_Autoloader::register();

// if using a framework that already uses an autoloading mechanism, like Yii for example, 
// you can register the autoloader like:
// Yii::registerAutoloader(array('ApifonApi_Autoloader', 'autoloader'), true);

// configuration object
$config = new ApifonApi_Config(array(
    'apiUrl'        => 'http://mail.apifon.com/api',
    'publicKey'     => 'PUBLIC-KEY-HERE',
    'privateKey'    => 'PRIVATE-KEY-HERE',
    
    // components
    'components' => array(
        'cache' => array(
            'class'     => 'ApifonApi_Cache_File',
            'filesPath' => dirname(__FILE__) . '/../ApifonApi/Cache/data/cache', // make sure it is writable by webserver
        )
    ),
));

// now inject the configuration and we are ready to make api calls
ApifonApi_Base::setConfig($config);

// start UTC
date_default_timezone_set('UTC');